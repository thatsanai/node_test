"use strict";


// encrypt
var crypto = require("crypto"),
    algorithm = "aes-256-ctr",
    secret = "d6F3Efeq";

exports.encrypt = (text) => {
    var cipher = crypto.createCipher(algorithm, secret);
    var crypted = cipher.update(text, "utf8", "hex");
    crypted += cipher.final("hex");
    return crypted;
}

exports.randomString = async (i) => {
    i = await this.opt(i, 48)
    let buffer = await crypto.randomBytes(i)
    return await buffer.toString("hex")
}

exports.responseMessages = (code, message, data) => {
    return {
        code,
        message,
        data
    }
}

module.exports.opt = async (options, def) => {
    return await options || def;
}

module.exports.getCurrentDate = () => {
    let curDate = new Date();
    return curDate.getFullYear() + "-" + (curDate.getMonth() + 1) + "-" + curDate.getDate();
};

const imageFilter = function (req, file, cb) {
    // Accept images only
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG)$/)) {
        req.fileValidationError = 'Only image files are allowed!';
        return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
};
exports.imageFilter = imageFilter;

