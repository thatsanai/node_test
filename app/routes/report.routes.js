/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Module dependencies.
 */


/**
 * Load app configurations
 */
const auth = require("../controllers/authentication.controller")
const report = require("../controllers/report.controller")

module.exports = function (app) {
    app.route("/report").get(auth.isAuthenticated, report.index);
    app.route("/report_score").get(auth.isAuthenticated, report.score);
};