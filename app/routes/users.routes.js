/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

const users = require("../../app/controllers/users.controller")
const auth = require("../controllers/authentication.controller")

module.exports = function (app) {
  app.route("/login")
    .get(users.formLogin)
    .post(auth.isProtect, users.login);

  app.route('/logout').get(users.logout)

  app.route("/forgotpassword")
    .get(users.formForgotPassword)
    .post(auth.isProtect, users.forgotPassword);
};

