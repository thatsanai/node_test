/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

const auth = require("../../controllers/authentication.controller")
const position = require("../../../app/controllers/management/position.controller")

module.exports = function (app) {

  /* 
  * /management/users/position
  */
  app.route("/management/users/position")
    .get(auth.isAuthenticated, position.findAll)
    .post(position.validate('create'), position.create)


  app.route("/management/users/position/:id")
    .get(auth.isAuthenticated, position.findOne)
    .put(position.validate('update'), position.update)
    .delete(position.validate('delete'), position.delete)

};

