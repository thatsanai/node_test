/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

const auth = require("../controllers/authentication.controller")
const patient = require("../controllers/patient.controller.js")

module.exports = function (app) {
  /* 
  * /patient
  */

  /**
   * Activity
   */
  app.route("/patient/:hn/activity").get(auth.isAuthenticated, patient.findActivity)

  /**
   * Message To MD
   */
  app.route("/patient/:hn/message").get(auth.isAuthenticated, patient.findMessageToMD)

  /**
   * Staff
   */
  app.route("/patient/:hn/staff").get(auth.isAuthenticated, patient.findStaff)

  /**
   * Need Button
   */
  app.route("/patient/:hn/request").get(auth.isAuthenticated, patient.findRequest)

  /**
   * Vital Sign
   */
  app.route("/patient/:hn/vitalsign").get(auth.isAuthenticated, patient.findVitalsign)

};

