/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Load app configurations
 */


const auth = require("../controllers/authentication.controller")
const health = require("../controllers/health.controller")


const path = require('path');
const multer = require('multer')
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/banner');
    },
    // By default, multer removes file extensions so let's add them back
    filename: function (req, file, cb) {
        cb(null, Date.now() + path.extname(file.originalname))
    }
});
var upload = multer({ storage: storage })



module.exports = function (app) {

    app.route("/health").get(auth.isAuthenticated, health.index);

    app.route("/health/create")
        .get(auth.isAuthenticated, health.formCreate)
        .post(upload.single('banner'), health.validate('create'), health.create);

    app.route("/health/:id")
        .get(auth.isAuthenticated, health.findOne)
        .put(upload.single('banner'), health.validate('update'), health.update)
        .delete(health.validate('delete'), health.delete)


};