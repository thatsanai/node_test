/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Module dependencies.
 */
var _ = require("lodash");
var glob = require("glob");

/**
 * Load app configurations
 */
const auth = require("../controllers/authentication.controller")
const packet = require("../controllers/packet/packet.controller")
const gohome = require("../controllers/packet/gohome.controller")

module.exports = function (app) {
    app.route("/packet").get(auth.isAuthenticated, packet.index)
    app.route("/gohome").get(auth.isAuthenticated, gohome.index)
};