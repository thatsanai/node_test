/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Module dependencies.
 */
var _ = require("lodash");
var glob = require("glob");

/**
 * Load app configurations
 */
const auth = require("../controllers/authentication.controller")
const core = require("../controllers/core.controller")

module.exports = function (app) {
  app.route("/").get(auth.isAuthenticated, core.index);

};