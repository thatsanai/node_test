"use strict";

/**
 * Get files by glob patterns
 */
const config = require("../../config/config.js"),
  db = require("../../config/db_connection")();

const logModel = require("../models/log.model")(db),
  userModel = require('../models/users.model')(db);

/**
 * Services
 */
exports.sendMail = (mail) => {
  let mailer = {
    host: config.mailer.host, //set to your host name or ip
    port: config.mailer.port, //25, 465, 587 depend on your
    secure: false, // use SSL
    ignoreTLS: true,
    tls: {
      rejectUnaythorized: false
    }
  };
  let smtpTransport = mailer.createTransport(mailer);
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) this.saveLog('sendMail()', JSON.stringify(error), JSON.stringify(mailer))
  });

  return true;
};

exports.saveLog = (_func, _err, _json) => {
  logModel.saveLog({ func_name: _func, msg_err: _err, data: _json }, (err) => {
    if (err) console.log(_func, this.getCurrentDate(), 'log fails')
    else console.log(_func, this.getCurrentDate(), 'log successfuly')
  })
};

// ###### Service Side
exports.verifyToken = async (req) => {
  const token = req.session.token || null;
  let result = await userModel.verifyToken({ token })
  result = await this.opt(result, null)
  if (!result.length) return await false
  return await true
};

/**
 * Helper
 */
let crypto = require("crypto"),
  algorithm = "aes-256-ctr",
  secret = "d6F3Efeq";

exports.encrypt = (text) => {
  let cipher = crypto.createCipher(algorithm, secret);
  let crypted = cipher.update(text, "utf8", "hex");
  crypted += cipher.final("hex");
  return crypted;
}

exports.randomString = async (i) => {
  i = await this.opt(i, 48)
  let buffer = await crypto.randomBytes(i)
  return await buffer.toString("hex")
}

exports.responseMessages = (code, message, data) => {
  return {
    code,
    message,
    data
  }
}

module.exports.opt = async (options, def) => {
  return await options || def;
}

module.exports.getCurrentDate = () => {
  let curDate = new Date();
  return curDate.getFullYear() + "-" + (curDate.getMonth() + 1) + "-" + curDate.getDate();
};



