/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
    let model = {
        saveLog: function (data, callback) {
            const result = db.query("INSERT INTO tb_log SET ?",
                [data],
                callback);
            return result;
        }
    };
    return model;
};