/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
  let model = {
    /**
     * GET USERS DATA
     */
    getUserByUsernamePassword: async function (user) {
      const result = db.query(
        `SELECT users.*, role_user.role_id, files.file_name,
        position.position_name_th, position.position_name_en
        FROM users 
        LEFT JOIN role_user ON (users.user_code = role_user.user_code) 
        LEFT JOIN position ON (users.position_id = position.position_id) 
        LEFT JOIN files ON (users.user_id = files.ref_id AND files.type = 'profile' AND file_type = 'thumnail')
        WHERE (username=? OR email=?) AND password=? AND users.isactive=? AND isdelete=? 
        LIMIT 1`,
        [user.username, user.username, user.password, 1, 0]
      );
      return await result;
    },

    /**
     * VERIFICATION USERS DATA
     */
    verifyToken: async function (user) {
      const result = db.query(
        `SELECT 1 FROM users WHERE last_token=? AND isactive=? AND isdelete=?`,
        [user.token, 1, 0]
      );
      return await result;
    },

    /**
     * UPDATE USERS DATA
     */
    stampTokenByUserID: async function (user) {
      const result = db.query(
        `UPDATE users SET last_token=?, last_login=NOW() WHERE user_id=?`,
        [user.token, user.user_id]
      );
      return await result;
    },

  };
  return model;
};
