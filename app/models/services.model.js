/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
  let model = {
    /**
     * GET DATA
     */
    getCountries: async function () {
      const result = db.query(
        `SELECT * FROM countries`,
        []
      );
      return await result
    },
    getProvinces: async function () {
      const result = db.query(
        `SELECT * FROM provinces`,
        []
      );
      return await result
    },
    getDistrictByProvince: async function (province) {
      const result = db.query(
        `SELECT * FROM districts WHERE province_id=?`,
        [province.province_id]
      );
      return await result
    },
    getSubDistrictsByDistrict: async function (district) {
      const result = db.query(
        `SELECT * FROM sub_districts WHERE district_id=?`,
        [district.district_id]
      );
      return await result
    },
    getPosition: async function () {
      const result = db.query(
        `SELECT * FROM position WHERE isactive=?`,
        [1]
      );
      return await result
    },
    getNationality: async function () {
      const result = db.query(
        `SELECT * FROM nationality`,
        []
      );
      return await result
    },
    getOrigin: async function () {
      const result = db.query(
        `SELECT * FROM origin`,
        []
      );
      return await result
    },
    getReligion: async function () {
      const result = db.query(
        `SELECT * FROM religion`,
        []
      );
      return await result
    },

    /**
     * UPDATE USERS DATA
     */
  };
  return model;
};
