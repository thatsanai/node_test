/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
    let model = {
        /**
         * GET Employess
         */
        getEmployess: async function (data) {
            const result = db.query(
                `SELECT  CONCAT(firstname, ' ',lastname) AS fullname, phone , email ,DATE_FORMAT(last_login, '%d %b %Y %H:%i') AS last_login 
                 FROM users
                 WHERE isactive=? AND bu=? `,
                [1, data.bu]
            );
            return await result;
        },
    };
    return model;
};
