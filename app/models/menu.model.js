/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
  let model = {
    getMenuByUserRole: async function (user) {
      const result = db.query(
        "SELECT * FROM v_menu WHERE user_code=? AND role_id=?",
        [user.user_code, user.role_id]
      );
      return await result;
    },
    getMenuByPath: async function (data) {
      const result = db.query(
        "SELECT * FROM menu_sub WHERE menu_sub_uri=?",
        [data.path]
      );
      return await result;
    },
    getAllMenu: async function () {
      const result = db.query(
        `SELECT menu_sub.menu_sub_id, menu_sub.menu_sub_name_th, menu_sub.menu_sub_name_en, menu_sub.menu_sub_sort,
        menu_sub.menu_id, menu.menu_name_th, menu.menu_name_en, menu.menu_sort
        FROM menu_sub
        INNER JOIN menu ON (menu_sub.menu_id = menu.menu_id)
        ORDER BY menu.menu_sort, menu_sub.menu_sub_sort`,
        []
      );
      return await result;
    },
    getMenuByRoleID: async function (role) {
      const result = db.query(
        `SELECT menu_sub.menu_sub_id, menu_sub.menu_sub_name_th, menu_sub.menu_sub_name_en, menu_sub.menu_sub_sort,
        menu_sub.menu_id, menu.menu_name_th, menu.menu_name_en, menu.menu_sort,
        IFNULL(is_view, 0) AS is_view, 
        IFNULL(is_create, 0) AS is_create, 
        IFNULL(is_update, 0) AS is_update, 
        IFNULL(is_delete, 0) AS is_delete
        FROM menu_sub
        INNER JOIN menu ON (menu_sub.menu_id = menu.menu_id)
        LEFT JOIN role_permission ON (menu_sub.menu_sub_id = role_permission.menu_sub_id AND role_permission.role_id = ?)
        ORDER BY menu.menu_sort, menu_sub.menu_sub_sort`,
        [role.id]
      );
      return await result;
    },
  };
  return model;
};
