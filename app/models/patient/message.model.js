/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
  let model = {
    /**
     * GET PATIENT DATA
     */
    findPatientByHN: async function (patient) {
      const result = db.query(
        `SELECT * FROM patients WHERE bu=? AND hn_number=? AND isactive=?`,
        [patient.bu, patient.hn_number, patient.isactive]
      );
      return await result;
    }

  };
  return model;
};
