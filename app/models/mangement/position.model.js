/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
  let model = {
    /**
     * GET POSITION DATA
     */
    getAllPosition: async function () {
      const result = db.query(
        `SELECT position.*, CONCAT(users.firstname, ' ', users.lastname) AS updateby, DATE_FORMAT(position.updatedatetime, '%d %b %Y %H:%i') AS updatedatetime
        FROM position 
        LEFT JOIN users ON (position.updateby = users.user_code)
        WHERE position.isactive=?`,
        [1]
      );
      return await result;
    },
    getPositionByID: async function (position) {
      const result = db.query(
        `SELECT position.*, CONCAT(users.firstname, ' ', users.lastname) AS updateby, DATE_FORMAT(position.updatedatetime, '%d %b %Y %H:%i') AS updatedatetime
        FROM position 
        LEFT JOIN users ON (position.updateby = users.user_code)
        WHERE position.position_id=? AND position.isactive=?`,
        [position.id, 1]
      );
      return await result;
    },

    /**
     * UPDATE POSITION DATA
     */
    updatePositionByID: async function (position, data) {
      const result = await db.query(
        `UPDATE position SET ? WHERE position_id=?`,
        [position, data.id]
      );
      return await result;
    },

    /**
     * UPDATE POSITION (IN-ACTIVE) DATA
     */
    deletePositionByID: async function (position) {
      const result = await db.query(
        `UPDATE position SET updateby=?, isactive=? WHERE position_id=?`,
        [position.user_logon, 0, position.position_id]
      );
      return await result;
    },


    /**
     * CREATE POSITION DATA
     */
    createPosition: async function (position) {
      const result = await db.query(
        `INSERT INTO position SET ?`,
        [position]
      );
      return await result;
    },

  };
  return model;
};
