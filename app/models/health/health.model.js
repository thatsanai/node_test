/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
module.exports = function (db) {
    let model = {
        /**
         *  GET 
         */
        getHealth: async function (data) {
            const result = db.query(
                `SELECT  health_content_id, titel_th, titel_en, titel_ara, titel_jp, DATE_FORMAT(updatedatetime, '%d %b %Y %H:%i') AS date 
                 FROM health_content
                 WHERE isactive=? AND bu=?`,
                [1, data.bu]
            );
            return await result;
        },

        getHealthByID: async function (data) {
            const result = db.query(
                `SELECT *
                 FROM health_content
                 WHERE health_content_id=?`,
                [data.health_content_id]
            );
            return await result;
        },

        /**
         *  CREATE
         */
        createHealth: async function (data) {
            const result = await db.query(
                `INSERT INTO health_content (
                titel_th,
                topic_th,
                highlights_th,
                health_content_th,
                titel_en,
                topic_en,
                highlights_en,
                health_content_en,
                titel_ara,
                topic_ara,
                highlights_ara,
                health_content_ara,
                titel_jp,
                topic_jp,
                highlights_jp,
                health_content_jp,
                banner,
                bu,
                isactive,
                createby,
                updateby)
              VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
                [data.titel_th,
                data.topic_th,
                data.highlights_th,
                data.health_content_th,
                data.titel_en,
                data.topic_en,
                data.highlights_en,
                data.health_content_en,
                data.titel_ara,
                data.topic_ara,
                data.highlights_ara,
                data.health_content_ara,
                data.titel_jp,
                data.topic_jp,
                data.highlights_jp,
                data.health_content_jp,
                data.banner,
                data.bu,
                data.isactive,
                data.createby,
                data.updateby]
            );
            return await result;
        },


        /**
         *  UPDATE
         */
        updateHealthByID: async function (data) {
            const result = await db.query(
                `UPDATE health_content SET
                titel_th=?, topic_th=?, highlights_th=?, health_content_th=?,
                titel_en=?, topic_en=?, highlights_en=?, health_content_en=?,
                titel_ara=?, topic_ara=?, highlights_ara=?, health_content_ara=?,
                titel_jp=?, topic_jp=?, highlights_jp=?, health_content_jp=?,
                bu=?,
                updateby=? 
                WHERE health_content_id=? `,
                [data.titel_th,
                data.topic_th,
                data.highlights_th,
                data.health_content_th,
                data.titel_en,
                data.topic_en,
                data.highlights_en,
                data.health_content_en,
                data.titel_ara,
                data.topic_ara,
                data.highlights_ara,
                data.health_content_ara,
                data.titel_jp,
                data.topic_jp,
                data.highlights_jp,
                data.health_content_jp,
                data.bu,
                data.updateby,
                data.health_content_id]
            );
            return await result;
        },

        updateBannerByID: async function (data) {
            const result = await db.query(
                `UPDATE health_content SET banner=?, updateby=? WHERE health_content_id=? `,
                [data.banner, data.updateby, data.health_content_id]
            );
            return await result;
        },


        /**
         * DELETE  
         */
        deleteHealth: async function (data) {
            const result = await db.query(
                `UPDATE health_content SET isactive=?, updateby=? WHERE health_content_id=? `,
                [data.isactive, data.updateby, data.health_content_id]
            );
            return await result;
        },
    };
    return model;
};
