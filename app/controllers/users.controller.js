/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

"use strict";

/**
 * Get files by glob patterns
 */
const { mailer } = require("../../config/config.js")
const { opt, responseMessages, encrypt, randomString, verifyToken } = require('../services/app')
const findOne = require('../utils/findOne')
const db = require("../../config/db_connection")()

/**
 * Module dependencies.
 */
const userModel = require('../models/users.model')(db)
const menuModel = require('../models/menu.model')(db)

exports.formLogin = async function (req, res) {
  const result = await verifyToken(req)
  if (result)
    return res.redirect('/');
  else
    return res.render('login');
};

/**
 * Signin after authentication
 */
exports.login = async function (req, res, next) {
  let body = req.body;
  let username = await opt(body.username, null)
  let password = await opt(body.password, null)
  if (!username) return res.json(await responseMessages(301, 'username or password incorrect'))

  if (!password) return res.json(await responseMessages(301, 'invalid password'))

  if (password.length < 8) return res.json(await responseMessages(301, 'Please enter password more than 8 charecter'))

  let result = await userModel.getUserByUsernamePassword({ username, password: encrypt(password) })

  if (!result.length) return res.json(await responseMessages(301, 'username or password incorrect'))
  result = await findOne(result)
  const token = await randomString(null)
  await userModel.stampTokenByUserID({ token, user_id: result.user_id })
  /**
   * Put session
   */
  const user_code = result.user_code
  const firstname = result.firstname
  const lastname = result.lastname
  const position = result.position_name_en
  const image_profile = result.file_name
  const bu = result.bu

  req.session.token = token
  req.session.userCode = user_code
  req.session.firstname = firstname
  req.session.lastname = lastname
  req.session.position = position
  req.session.imageProfile = image_profile
  req.session.bu = bu

  return res.json(await responseMessages(200, 'OK'))
};

exports.formForgotPassword = function (req, res) {
  return res.render("forgotpassword", {
    request: req
  });
};

exports.forgotPassword = function (req, res, next) {
  let body = req.body;

  let email = body.email || null;
  if (!email)
    return res.status(200).json({ code: 303, message: "invalid email." });

  user_model.verifyAccountByEmail(
    {
      email: email,
      status: "active"
    },
    (err, account) => {
      if (account == undefined || !account.length)
        return res.status(200).json({ code: 304, message: "email not found" });

      let result = findOne(account);
      let random_password = Math.floor(1000 + Math.random() * 9000).toString();
      // update password
      user_model.changePassword(
        {
          account_code: result.account_code,
          password: encrypt(random_password)
        },
        (err, update) => {
          if (err)
            return res
              .status(200)
              .json({ code: 305, message: "an error occurred." });
          // email
          let mail = {
            from: mailer.from, //from email (option)
            to: "noppichaia@gmail.com", //to email (require)
            subject: "Forgot Password | Samitivej PACE", //subject
            html:
              `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>สมิติเวช</title>
                <style>
                    h4 {
                        font-size: 22px;
                        margin-top: 0;
                        margin-bottom: 1rem;
                    }
                    h5 {
                        font-size: 18px;
                        margin-top: 0;
                        margin-bottom: 1rem;
                    }
                    .div-front {
                        width: 40%;
                    }
                    .div-back {
                        width: 60%;
                    }

                    @media (max-width:500px) {
                        .div-front {
                            width: 100%;
                        }
                        .div-back {
                            width: 100%;
                        }
                    }
                </style>
            </head>
            <body>
                <main style="width:100%; max-width:768px; margin:0 auto; border:1px solid #ccc;">
                    <div style="width:100%;">
                        <img src="cid:head@icon.ee" style="width:100%;">
                    </div>
                    <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                        Dear ` +
              result.department_en +
              `
                    </div>
                    <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                        You have asked to recover your password for Samitivej PACE system. The latest password you have set for this account is shown as below.
                    </div>
                    <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                        Here is the password you need to login to an account:
                    </div>
                    <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                        <h5>` +
              random_password +
              `</h5>
                     </div>
                     <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                         Thank you for using Samitivej PACE.
                     </div>
                     <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                         Best Regards,
                     </div>
                     <div style="width:90%; margin:0 auto; display: flex; flex-wrap: wrap;">
                         Samitivej PACE Team
                     </div>
                    <div style="width:100%; text-align: center; margin-top: 15px; margin-bottom:15px;">
                        <p class="text-center" style="font-family: 'CS PraKas'; font-weight: 600;">
                            Copyright © 2018 Samitivej Hospital, All rights reserved.
                        </p>
                    </div>
                </main>
            </body>
            </html>`, //email body
            attachments: [
              {
                filename: "head.png",
                path:
                  "http://pace.samitivejhospitals.com/img/img_mail/head.png",
                cid: "head@icon.ee" //same cid value as in the html img src
              }
            ]
          };
          send_email(mail);

          return res.status(200).json({
            code: 200,
            message: "OK"
          });
        }
      );
    }
  );
};
/**
 * logout
 */
exports.logout = function (req, res) {
  req.session.destroy(() => {
    req.session = null;
    return res.redirect("/login");
  });
};
