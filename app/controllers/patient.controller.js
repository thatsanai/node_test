/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

"use strict";

/**
 * Get files by glob patterns
 */
const { opt } = require('../services/app')
const findOne = require('../utils/findOne')
const db = require("../../config/db_connection")()
const { body } = require('express-validator')
const { TAB } = require('../constants/tab')
const { REQ } = require('../constants/icon')
/**
 * Module dependencies.
 */
const messageModel = require('../models/patient/message.model')(db)
const serviceModel = require('../models/services.model')(db)

/**
 * 
 */
exports.findActivity = async function (req, res) {
  const user = {
    hn_number: await opt(req.params.hn, null),
    bu: req.session.bu || null,
    isactive: 1
  }
  const profile = await messageModel.findPatientByHN(user)
  return res.render("patient/request", {
    user: Object.assign(req.session, { route: req.path.split('/')[3] }),
    tab: TAB,
    patient: await findOne(profile) || []
  });
};

exports.findVitalsign = async function (req, res) {
  const user = {
    hn_number: await opt(req.params.hn, null),
    bu: req.session.bu || null,
    isactive: 1
  }
  const profile = await messageModel.findPatientByHN(user)
  return res.render("patient/request", {
    user: Object.assign(req.session, { route: req.path.split('/')[3] }),
    tab: TAB,
    patient: await findOne(profile) || []
  });
};

exports.findRequest = async function (req, res) {
  const user = {
    hn_number: await opt(req.params.hn, null),
    bu: req.session.bu || null,
    isactive: 1
  }
  const profile = await messageModel.findPatientByHN(user)
  return res.render("patient/request", {
    user: Object.assign(req.session, { route: req.path.split('/')[3] }),
    icon: REQ,
    tab: TAB,
    patient: await findOne(profile) || []
  });
};

exports.findStaff = async function (req, res) {
  const user = {
    hn_number: await opt(req.params.hn, null),
    bu: req.session.bu || null,
    isactive: 1
  }
  const profile = await messageModel.findPatientByHN(user)
  return res.render("patient/request", {
    user: Object.assign(req.session, { route: req.path.split('/')[3] }),
    tab: TAB,
    patient: await findOne(profile) || []
  });
};

exports.findMessageToMD = async function (req, res) {
  const user = {
    hn_number: await opt(req.params.hn, null),
    bu: req.session.bu || null,
    isactive: 1
  }
  const profile = await messageModel.findPatientByHN(user)
  return res.render("patient/message", {
    user: Object.assign(req.session, { route: req.path.split('/')[3] }),
    tab: TAB,
    patient: await findOne(profile) || []
  });
};

/**
 * Validate Body
 * exists() => required
 * isEmail() => Email format
 * optional()
 * isInt()
 * isIn(['enabled', 'disabled'])
 */
exports.validate = (method) => {
  switch (method) {
    case 'update': {
      return [
        /**
         * Profile Information
         */
        body('email')
          .isEmail().withMessage('Invalid email')
          .exists().withMessage('email doesnt exists'),
        body('firstname').isLength({ min: 4 }).withMessage('Must be at least 4 chars long')
          .exists().withMessage('firstname doesnt exists'),
        body('lastname').isLength({ min: 4 }).withMessage('Must be at least 4 chars long')
          .exists().withMessage('lastname doesnt exists'),
        body('phone')
          .optional().isLength({ min: 12 }).withMessage('Must be at least 12 chars long'),
        body('position_id').isInt()
          .exists().withMessage('position doesnt exists'),
        body('salary').optional()
          .isDecimal(),
        /**
         * Card Information
         */
        body('birthday').optional(),
        body('nationality_id').optional().isInt(),
        body('origin_id').optional().isInt(),
        body('religion_id').optional().isInt()
      ]
    }
  }
}