/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get unique error field name
 */
var getUniqueErrorMessage = function (err) {
  var output;

  try {
    var fieldName = err.err.substring(
      err.err.lastIndexOf(".$") + 2,
      err.err.lastIndexOf("_1")
    );
    output =
      fieldName.charAt(0).toUpperCase() +
      fieldName.slice(1) +
      " already exists";
  } catch (ex) {
    output = "Unique field already exists";
  }

  return output;
};

/**
 * Get the error message from error object
 */
exports.getErrorMessage = function (_code = 200) {
  let _message = "";
  switch (_code) {
    case 2202:
      _message = 'something went wrong!'; // getUniqueErrorMessage(_code)
      break;
    default:
      _message = "OK";
  }

  return {
    code: _code,
    message: _message
  };
};
