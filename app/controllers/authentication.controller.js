/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

"use strict";

/**
 * Get files by glob patterns
 */
const { verifyToken } = require('../services/app')

exports.isAuthenticated = async (req, res, next) => {
  // do any checks you want to in here
  // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
  // you can do this however you want with whatever variables you set up
  const token = req.session.token || null

  // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  if (!token) return res.redirect('/login')
  const result = await verifyToken(req)
  if (!result) return res.redirect('/login')
  return next();
};

exports.isProtect = async (req, res, next) => {
  // do any checks you want to in here
  // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
  // you can do this however you want with whatever variables you set up
  const token = req.session.token || null

  // IF A USER ISN'T LOGGED IN, THEN REDIRECT THEM SOMEWHERE
  // if (!token) return res.redirect('/login')
  return next()

};

