/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get files by glob patterns
 */
const db = require("../../config/db_connection")()

/**
 * Module dependencies.
 */
const employessModel = require('../models/employess/employess.model')(db)


exports.index = async function (req, res) {
    const bu = req.session.bu
    //console.log(bu)
    // var hour = 60000
    // req.session.cookie.expires = new Date(Date.now() + hour)
    // req.session.cookie.maxAge = hour
    // console.log('cookie.expires : ' + req.session.cookie.expires)
    // console.log('cookie.maxAge : ' + req.session.cookie.maxAge)
    let employess = await employessModel.getEmployess({ bu })
    return res.render("employess/index", {
        user: req.session,
        employess: employess
    });
};

