/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get files by glob patterns
 */
const db = require("../../config/db_connection")()

/**
 * Module dependencies.
 */
const employessModel = require('../models/employess/employess.model')(db)


exports.index = async function (req, res) {
  const bu = req.session.bu
  let employess = await employessModel.getEmployess({ bu })
  return res.render("employess/index", {
    user: req.session,
    employess: employess
  });
};