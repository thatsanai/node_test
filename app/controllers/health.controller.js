/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get files by glob patterns
 */

const services = require('../../hepler/services')
const findOne = require('../../hepler/findOne')
const db = require('../../config/db_connection')()
const { body, validationResult } = require('express-validator')

/**
 * Module dependencies.
 */
const healthModel = require('../models/health/health.model')(db)

const prefix = 'health/'

exports.index = async function (req, res) {
    const bu = req.session.bu
    let healths = await healthModel.getHealth({ bu })
    return res.render("health/index", {
        user: req.session,
        health: healths
    });
};

exports.findOne = async function (req, res) {
    let health_content_id = req.params.id
    const renderFile = req.query.action ? (prefix + 'update') : (prefix + 'visit')
    let health = await healthModel.getHealthByID({ health_content_id })
    return res.render(renderFile, {
        user: req.session,
        health: await findOne(health)
    });
};


exports.formCreate = async function (req, res) {
    return res.render("health/create", {
        user: req.session
    });
};

// Create
exports.create = async function (req, res, next) {

    const userLogon = req.session.userCode
    const bu = req.session.bu

    let banner = `/images/profile.png`
    if (req.file)
        banner = `/uploads/banner/${req.file.filename}`
    else
        return res.json(await services.responseMessages(301, 'banner is not found', [{
            msg: 'banner is not found',
            param: "alert"
        }]))


    const {
        titel_th,
        topic_th,
        highlights_th,
        health_content_th,
        titel_en,
        topic_en,
        highlights_en,
        health_content_en,
        titel_ara,
        topic_ara,
        highlights_ara,
        health_content_ara,
        titel_jp,
        topic_jp,
        highlights_jp,
        health_content_jp
    } = req.body
    try {
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
        if (!errors.isEmpty()) {
            const extractedErrors = []
            errors.array().map(err => extractedErrors.push({
                ['value']: err.value,
                ['msg']: err.msg,
                ['param']: err.param,
                ['location']: err.location
            }))
            return res.json(await services.responseMessages(301, 'is invalid', extractedErrors))
        }

        const create = await healthModel.createHealth({
            titel_th,
            topic_th,
            highlights_th,
            health_content_th,
            titel_en,
            topic_en,
            highlights_en,
            health_content_en,
            titel_ara,
            topic_ara,
            highlights_ara,
            health_content_ara,
            titel_jp,
            topic_jp,
            highlights_jp,
            health_content_jp,
            banner,
            bu,
            isactive: 1,
            createby: userLogon,
            updateby: userLogon
        })

        if (!create) {
            return res.json(await services.responseMessages(744, create))
        }
        return res.json(await services.responseMessages(200, 'OK'))
    } catch (err) {
        console.log(err)
        return res.json(await services.responseMessages(500, 'an error has occurred'))
    }
};

// Update
exports.update = async function (req, res, next) {

    let health_content_id = req.params.id
    const userLogon = req.session.userCode
    const bu = req.session.bu

    const { titel_th,
        topic_th,
        highlights_th,
        health_content_th,
        titel_en,
        topic_en,
        highlights_en,
        health_content_en,
        titel_ara,
        topic_ara,
        highlights_ara,
        health_content_ara,
        titel_jp,
        topic_jp,
        highlights_jp,
        health_content_jp,
        banner_change } = req.body

    try {
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
        if (!errors.isEmpty()) {
            const extractedErrors = []
            errors.array().map(err => extractedErrors.push({
                ['value']: err.value,
                ['msg']: err.msg,
                ['param']: err.param,
                ['location']: err.location
            }))
            return res.json(await services.responseMessages(301, 'is invalid', extractedErrors))
        }

        let update = await healthModel.updateHealthByID({
            titel_th,
            topic_th,
            highlights_th,
            health_content_th,
            titel_en,
            topic_en,
            highlights_en,
            health_content_en,
            titel_ara,
            topic_ara,
            highlights_ara,
            health_content_ara,
            titel_jp,
            topic_jp,
            highlights_jp,
            health_content_jp,
            bu,
            updateby: userLogon,
            health_content_id
        })


        if (!update) {
            return res.json(await services.responseMessages(744, update))
        }

        if (banner_change == '1') {
            let banner = `/images/profile.png`
            if (req.file)
                banner = `/uploads/banner/${req.file.filename}`
            await healthModel.updateBannerByID({ banner, updateby: userLogon, health_content_id })
        }

        return res.json(await services.responseMessages(200, 'OK'))
    } catch (err) {
        console.log(err)
        return res.json(await services.responseMessages(500, 'an error has occurred'))
    }
};


// Delete
exports.delete = async function (req, res, next) {
    const health_content_id = req.params.id
    const userLogon = req.session.userCode
    try {
        const update = await healthModel.deleteHealth({ isactive: "0", updateby: userLogon, health_content_id })
        if (!update)
            return res.json(await services.responseMessages(744, update))
        return res.json(await services.responseMessages(200, 'OK'))

    } catch (err) {
        console.log(err)
        return res.json(await services.responseMessages(500, 'an error has occurred'))
    }
};



exports.validate = (method) => {
    switch (method) {
        case 'create': {
            return [
                body('titel_th', 'titel th is required').not().isEmpty(),
                body('titel_en', 'titel en is required').not().isEmpty(),
                body('titel_ara', 'titel ara is required').not().isEmpty(),
                body('titel_jp', 'titel jp is required').not().isEmpty(),
                body('topic_th', 'topic th is required').not().isEmpty(),
                body('topic_en', 'topic en is required').not().isEmpty(),
                body('topic_ara', 'topic ara is required').not().isEmpty(),
                body('topic_jp', 'topic jp is required').not().isEmpty(),
                body('highlights_th', 'highlights th is required').not().isEmpty(),
                body('highlights_en', 'highlights en is required').not().isEmpty(),
                body('highlights_ara', 'highlights ara is required').not().isEmpty(),
                body('highlights_jp', 'highlights jp is required').not().isEmpty(),
                body('health_content_th', 'health content th is required').not().isEmpty(),
                body('health_content_en', 'health content en is required').not().isEmpty(),
                body('health_content_ara', 'health content ara is required').not().isEmpty(),
                body('health_content_jp', 'health content jp is required').not().isEmpty()


            ]
        }
        case 'update': {
            return [
                body('titel_th', 'titel th is required').not().isEmpty(),
                body('titel_en', 'titel en is required').not().isEmpty(),
                body('titel_ara', 'titel ara is required').not().isEmpty(),
                body('titel_jp', 'titel jp is required').not().isEmpty(),
                body('topic_th', 'topic th is required').not().isEmpty(),
                body('topic_en', 'topic en is required').not().isEmpty(),
                body('topic_ara', 'topic ara is required').not().isEmpty(),
                body('topic_jp', 'topic jp is required').not().isEmpty(),
                body('highlights_th', 'highlights th is required').not().isEmpty(),
                body('highlights_en', 'highlights en is required').not().isEmpty(),
                body('highlights_ara', 'highlights ara is required').not().isEmpty(),
                body('highlights_jp', 'highlights jp is required').not().isEmpty(),
                body('health_content_th', 'health content th is required').not().isEmpty(),
                body('health_content_en', 'health content en is required').not().isEmpty(),
                body('health_content_ara', 'health content ara is required').not().isEmpty(),
                body('health_content_jp', 'health content jp is required').not().isEmpty(),
            ]
        }
        case 'delete': {
            return []
        }
    }
}