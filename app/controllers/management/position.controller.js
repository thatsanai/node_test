/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

"use strict";

/**
 * Get files by glob patterns
 */
const { opt, responseMessages } = require('../../services/app')
const findOne = require('../../utils/findOne')
const db = require("../../../config/db_connection")()
const { body, validationResult } = require('express-validator')
/**
 * Module dependencies.
 */
const positionModel = require('../../models/mangement/position.model')(db)

/**
 * Declare variable.
 */
const prefix = 'management/users/position'

/**
 * View Position
 */
exports.findAll = async function (req, res) {
  const page = await opt(req.page, null)
  const position = await positionModel.getAllPosition()
  const renderFile = req.query.action ? (prefix + '/create') : (prefix + '/index')
  if (!position) throw position
  return res.render(renderFile, {
    user: req.session,
    page,
    position: position
  })
};

/**
 * Form view && Update Position By Position ID
 */
exports.findOne = async function (req, res) {
  const page = await opt(req.page, null)
  const position_id = await opt(req.params.id, null)
  const position = await positionModel.getPositionByID({ id: position_id })
  if (!position.length) return res.redirect('/' + prefix)
  return res.render((prefix + '/update'), {
    user: req.session,
    page,
    position: await findOne(position)
  })
};

/**
 * Create Position
 */
exports.create = async function (req, res, next) {
  const userLogon = req.session.userCode

  /**
   * Body
   */
  const { position_name_th, position_name_en } = req.body

  try {
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
    if (!errors.isEmpty())
      return res.json(await responseMessages(301, 'is invalid', errors.array()))

    /**
     * Create position
     */
    await positionModel.createPosition({
      position_name_th, position_name_en,
      createby: userLogon
    })

    return res.json(await responseMessages(200, 'OK'))
  } catch (err) {
    console.log(err)
    return res.json(await responseMessages(500, 'an error has occurred'))
  }

};

/**
 * Update Position
 */
exports.update = async function (req, res, next) {
  let position_id = req.params.id
  const userLogon = req.session.userCode

  try {
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
    if (!errors.isEmpty())
      return res.json(await responseMessages(301, 'is invalid', errors.array()))

    /**
     * Body Information
     */
    const { position_name_th, position_name_en } = req.body

    /**
     * Update
     */
    const update = await positionModel.updatePositionByID({
      position_name_th, position_name_en, updateby: userLogon
    }, { id: position_id })

    if (!update)
      return res.json(await responseMessages(744, update))

    return res.json(await responseMessages(200, 'OK'))
  } catch (err) {
    console.log(err)
    return res.json(await responseMessages(500, 'an error has occurred'))
  }

};

/**
 * Delete Position
 */
exports.delete = async function (req, res, next) {
  const position_id = req.params.id
  const userLogon = req.session.userCode

  try {
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions
    if (!errors.isEmpty())
      return res.json(await responseMessages(301, 'delete is invalid', errors.array()))

    const remove = await positionModel.deletePositionByID({ position_id: position_id, user_logon: userLogon })
    if (!remove)
      return res.json(await responseMessages(744, remove))

    return res.json(await responseMessages(200, 'OK'))

  } catch (err) {
    console.log(err)
    return res.json(await responseMessages(500, 'an error has occurred'))
  }
};


/**
 * Validate Body
 * exists() => required
 * isEmail() => Email format
 * optional()
 * isInt()
 * isIn(['enabled', 'disabled'])
 */
exports.validate = (method) => {
  switch (method) {
    case 'create': {
      return [
        /**
         * Body
         */
        body('position_name_th', 'Position (TH) doesnt exists')
          .isLength({ min: 4 }).withMessage('Must be at least 4 chars long')
          .exists(),
        body('position_name_en', 'Position (EN) doesnt exists')
          .isLength({ min: 4 }).withMessage('Must be at least 4 chars long')
          .exists()
      ]
    }
    case 'update': {
      return [
        /**
         * Body
         */
        body('position_name_th', 'Position (TH) doesnt exists').isLength({ min: 4 }).exists(),
        body('position_name_en', 'Position (EN) doesnt exists').isLength({ min: 4 }).exists()
      ]
    }
    case 'delete': {
      return []
    }
  }
}