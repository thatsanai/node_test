/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get files by glob patterns
 */

/**
 * Module dependencies.
 */


exports.index = function (req, res) {
    return res.render("report/index", {
        user: req.session
    });
};


exports.score = function (req, res) {
    return res.render("report/score", {
        user: req.session
    });
};

