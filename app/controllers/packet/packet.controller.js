/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Get files by glob patterns
 */

/**
 * Module dependencies.
 */

exports.index = async function (req, res) {
    return res.render("packet/index", {
        user: req.session,
    });
};

