exports.TAB = [{
    route: 'activity',
    name: 'กิจวัตรประจำวัน',
    icon: '/images/patient_tab_1.png',
    url: '/patient/##-##-######/activity',
    badge: 0
},
{
    route: 'vitalsign',
    name: 'สัญญาณชีพ',
    icon: '/images/patient_tab_2.png',
    url: '/patient/##-##-######/vitalsign',
    badge: 0
},
{
    route: 'request',
    name: 'ความต้องการผู้ป่วย',
    icon: '/images/patient_tab_3.png',
    url: '/patient/##-##-######/request',
    badge: 0
},
{
    route: 'staff',
    name: 'เจ้าหน้าที่',
    icon: '/images/patient_tab_4.png',
    url: '/patient/##-##-######/staff',
    badge: 0
},
{
    route: 'message',
    name: 'ฝากข้อความถึงแพทย์',
    icon: '/images/patient_tab_5.png',
    url: '/patient/##-##-######/message',
    badge: 0
}];
