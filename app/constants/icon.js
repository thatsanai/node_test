exports.REQ = {
    'hungry': {
        'red': '/images/icon_status_demand/path_red.png',
        'yellow': '/images/icon_status_demand/path_yellow.png',
        'green': '/images/icon_status_demand/path_green.png'
    },
    'pain': {
        'red': '/images/icon_status_demand/pain.png',
        'yellow': '/images/icon_status_demand/pain.png',
        'green': '/images/icon_status_demand/pain.png'
    },
    'nurse': {
        'red': '/images/icon_status_demand/nurse_red.png',
        'yellow': '/images/icon_status_demand/nurse_yellow.png',
        'green': '/images/icon_status_demand/nurse_green.png'
    }
}

exports.ICON = {
    'home': '/images/home.png',
}
