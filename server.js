/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
'use strict';

/**
 * Module dependencies.
 */
var config = require('./config/config');

/**
 * Main application entry file.
 * Please note that the order of loading is important.
 */


// Init the express application
var app = require('./config/express')();

// // Start the app by listening on <port>
app.listen(config.port);

// // Expose app
exports = module.exports = app;

// // Logging initialization
console.log('application started on port ' + config.port);
