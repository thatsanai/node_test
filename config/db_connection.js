/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */

// ######### MYSQL
const mysql = require("mysql"),
  config = require("./config.js"),
  util = require('util');

const dbConfig = {
  host: config.db.host,
  user: config.db.user,
  password: config.db.password,
  database: config.db.database,
}
module.exports = function () {
  let pool = mysql.createPool(dbConfig);
  pool.query = util.promisify(pool.query)
  return pool;
};

