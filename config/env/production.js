/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

module.exports = {
  port: 3000,
  db: {
    queueLimit: 0, // unlimited queueing
    connectionLimit: 0, // unlimited connections 
    multipleStatements: true,
    host: "103.58.148.212",
    user: "admin_theme",
    password: "Theme#2019",
    database: "admin_theme",
    debug: false
  },
  app: {
    title: "Enco EI",
    description: "Enco EI software",
    keywords: "Enco EI"
  },
  mailer: {
    from: "name <username@mail.com>",
    host: "smtp.gmail.com",
    port: 465, //25, 465, 587 depend on your
    auth: {
      user: "username@mail.com", //user account
      pass: "password" //user password
    }
  },
  onesignal: {
    app_id: "",
    user_auth_key: "",
    app_auth_key: ""
  },
  bitly: {
    client_id: "",
    client_secret: "",
    token: ""
  }
};
