/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
'use strict';

module.exports = {
	sessionSecret: '$am!t!vejWardTrack!ngadmin',
	templates: {
		head: 'app/views/templates/head',
		navbar: 'app/views/templates/navbar',
		slidebar: 'app/views/templates/slidebar',
		header: 'app/views/templates/header',
		footer: 'app/views/templates/footer',
		script: 'app/views/templates/script',
		pagination: 'app/views/templates/pagination'
	},
	assets: {
		lib: {
			css: [
				'public/css/base.css',
				'public/css/style.css',
				'public/css/circle.css',
			],
			js: [
				// CORE
				'https://code.jquery.com/jquery-3.3.1.min.js',
				'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.bundle.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js',
				'https://cdn.jsdelivr.net/npm/metismenu',
				'public/js/scripts-init/app.js',
				'public/js/scripts-init/demo.js',

				// CHARTS
				// Apex Charts
				'public/js/vendors/charts/apex-charts.js',
				'public/js/scripts-init/charts/apex-charts.js',
				'public/js/scripts-init/charts/apex-series.js',

				// Sparklines
				'public/js/vendors/charts/charts-sparklines.js',
				'public/js/scripts-init/charts/charts-sparklines.js',

				// Chart.js
				'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js',
				'public/js/scripts-init/charts/chartsjs-utils.js',
				'public/js/scripts-init/charts/chartjs.js',

				// FORMS
				// Clipboard
				'public/js/vendors/form-components/clipboard.js',
				'public/js/scripts-init/form-components/clipboard.js',

				// Datepickers
				'public/js/vendors/form-components/datepicker.js',
				'public/js/vendors/form-components/daterangepicker.js',
				'public/js/vendors/form-components/moment.js',
				'public/js/scripts-init/form-components/datepicker.js',

				// Multiselect
				'public/js/vendors/form-components/bootstrap-multiselect.js',
				'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',
				'public/js/scripts-init/form-components/input-select.js',

				// Form Validation
				'public/js/vendors/form-components/form-validation.js',
				'public/js/scripts-init/form-components/form-validation.js',

				// Form Wizard
				'public/js/vendors/form-components/form-wizard.js',
				'public/js/scripts-init/form-components/form-wizard.js',

				// Input Mask
				'public/js/vendors/form-components/input-mask.js',
				'public/js/scripts-init/form-components/input-mask.js',

				// RangeSlider
				'public/js/vendors/form-components/wnumb.js',
				'public/js/vendors/form-components/range-slider.js',
				'public/js/scripts-init/form-components/range-slider.js',

				// Textarea Autosize
				'public/js/vendors/form-components/textarea-autosize.js',
				'public/js/scripts-init/form-components/textarea-autosize.js',

				// Toggle Switch 
				'public/js/vendors/form-components/toggle-switch.js',

				// COMPONENT

				// BlockUI
				'public/js/vendors/blockui.js',
				'public/js/scripts-init/blockui.js',

				// Calendar
				'public/js/vendors/calendar.js',
				'public/js/scripts-init/calendar.js',

				// Slick Carousel
				'public/js/vendors/carousel-slider.js',
				'public/js/scripts-init/carousel-slider.js',

				// Circle Progress
				'public/js/vendors/circle-progress.js',
				'public/js/scripts-init/circle-progress.js',

				// CountUp
				'public/js/vendors/count-up.js',
				'public/js/scripts-init/count-up.js',

				// Cropper
				'public/js/vendors/cropper.js',
				'public/js/vendors/jquery-cropper.js',
				'public/js/scripts-init/image-crop.js',


				// Guided Tours
				'public/js/vendors/guided-tours.js',
				'public/js/scripts-init/guided-tours.js',

				// Ladda Loading Buttons
				'public/js/vendors/ladda-loading.js',
				'public/js/vendors/spin.js',
				'public/js/scripts-init/ladda-loading.js',

				// Rating
				'public/js/vendors/rating.js',
				'public/js/scripts-init/rating.js',

				// Perfect Scrollbar
				'public/js/vendors/scrollbar.js',
				'public/js/scripts-init/scrollbar.js',

				// Toast
				'https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js',
				'public/js/scripts-init/toastr.js',

				// SweetAlert
				'https://cdn.jsdelivr.net/npm/sweetalert2@8',
				'public/js/scripts-init/sweet-alerts.js',

				// Tree View
				'public/js/vendors/treeview.js',
				'public/js/scripts-init/treeview.js',

				// TABLES
				// DataTable
				'https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js',
				'https://cdn.jsdelivr.net/npm/datatables.net-bs4@1.10.19/js/dataTables.bootstrap4.min.js',
				'https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js',
				'https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js',
				'https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js',
				'https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js',

				// Bootstrap Table
				'public/js/vendors/tables.js',

				// Tables Ini
				'public/js/scripts-init/tables.js',
				'public/js/scripts-init/services.js',

			]
		},
		css: [
			// public/modules/**/css/*.css
		]
	}
};
