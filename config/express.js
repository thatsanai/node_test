/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Module dependencies.
 */
var fs = require('fs'),
  express = require("express"),
  https = require("https"),
  path = require("path"),
  cookieParser = require("cookie-parser"),
  logger = require("morgan"),
  compress = require("compression"),
  session = require("express-session"),
  config = require("./config"),
  helmet = require("helmet"),
  bodyParser = require("body-parser"),
  methodOverride = require('method-override'),
  i18n = require("i18n-express");

module.exports = function () {
  /**
   * Initialize express app
   */
  var app = express();
  app.set("base_url", "wardtracking");

  /**
   * Globbing model files
   */
  config.getGlobbedFiles("./app/models/**/*.js").then(response => {
    response.forEach(function (modelPath) {
      require(path.resolve(modelPath))(app);
    });
  });

  /**
   * Setting application local variables
   */
  app.locals.title = config.app.title;
  app.locals.description = config.app.description;
  app.locals.keywords = config.app.keywords;

  /**
   * Layout
   */
  app.locals.head = path.resolve(config.templates.head)
  app.locals.header = path.resolve(config.templates.header)
  app.locals.navbar = path.resolve(config.templates.navbar)
  app.locals.slidebar = path.resolve(config.templates.slidebar)
  app.locals.footer = path.resolve(config.templates.footer)
  app.locals.script = path.resolve(config.templates.script)
  app.locals.pagination = path.resolve(config.templates.pagination)

  /**
   * Assets JS/CSS
   */
  app.locals.cssFiles = []
  app.locals.jsFiles = []

  config.getCSSAssets().then(item => {
    app.locals.cssFiles = item;
  });
  config.getJavaScriptAssets().then(item => {
    app.locals.jsFiles = item;
  });

  /**
   * Passing the request url to environment locals
   */
  app.use(function (req, res, next) {
    res.locals.url = req.protocol + "://" + req.headers.host + req.url;
    res.locals.path = req.url;
    next();
  });

  /**
   * Language
   */
  app.use(
    i18n({
      translationsPath: path.join(__dirname, "locals"), // <--- use here. Specify translations files path.
      siteLangs: ["en", "th"],
      textsVarName: "_"
    })
  );

  /**
   * methodOverride
   */
  app.use(methodOverride('_method'));

  /**
   * Should be placed before express.static
   */
  app.use(
    compress({
      filter: function (req, res) {
        return /json|text|javascript|css/.test(res.getHeader("Content-Type"));
      },
      level: 9
    })
  );

  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  /**
   * Showing stack errors
   */
  app.set("showStackError", true);

  // Set swig as the template engine
  //   app.engine("server.view.html", consolidate[config.templateEngine]);

  // Set views path and view engine
  app.set("view engine", "ejs"); // pug
  app.set("views", "./app/views");

  // Environment dependent middleware
  if (process.env.NODE_ENV === "development") {
    // Enable logger (morgan)
    // app.use(logger("dev"));

    // Disable views cache
    app.set("view cache", false);
  } else if (process.env.NODE_ENV === "production") {
    app.locals.cache = "memory";
  }

  // Request body parsing middleware should be above methodOverride
  app.use(
    bodyParser.urlencoded({
      extended: false
    })
  );

  app.use(bodyParser.json());
  //   app.use(methodOverride());

  // CookieParser should be above session
  app.use(cookieParser(config.sessionSecret));

  // Express MongoDB session storage
  app.use(
    session({
      saveUninitialized: true,
      resave: false,
      secret: config.sessionSecret,
      rolling: true, //Reset the cookie Max-Age on every request
      cookie: { secure: false }, // , maxAge: 60000  maxAge is set to 60000 (one minute)
    })
  );

  // use passport session
  //   app.use(passport.initialize());
  //   app.use(passport.session());

  // connect flash for flash messages
  //   app.use(flash());

  // Use helmet to secure Express headers
  // app.use(helmet.xframe());
  app.use(helmet.xssFilter());
  // app.use(helmet.nosniff());
  // app.use(helmet.ienoopen());
  app.disable("x-powered-by");

  // Setting the app router and static folder
  app.use(express.static(path.resolve("./public")));
  // Globbing routing files
  config.getGlobbedFiles("./app/routes/**/*.js").then(response => {
    response.forEach(function (routePath) {
      require(path.resolve(routePath))(app);
    });
  });
  // require(path.resolve("./app/routes/index.js"))(app);

  // Assume 'not found' in the error msgs is a 404. this is somewhat silly, but valid, you can do whatever you like, set properties, use instanceof etc.
  app.use(function (err, req, res, next) {
    // If the error object doesn't exists
    if (!err) return next();

    // Log it
    console.error(err.stack);

    // Error page
    res.status(500).render("error", {
      error: err.stack
    });
  });

  app.use(express.static(__dirname + '/public'))

  // Assume 404 since no middleware responded
  // app.use(function (req, res) {
  //   res.status(404).render("error", {
  //     url: req.originalUrl,
  //     error: "Not Found"
  //   });
  // });

  if (process.env.NODE_ENV === "secure") {
    // Log SSL usage
    console.log("Securely using https protocol");

    // Load SSL key and certificate
    var privateKey = fs.readFileSync("./config/sslcerts/cert.key", "utf8");
    var certificate = fs.readFileSync("./config/sslcerts/cert.crt", "utf8");

    // Create HTTPS Server
    var httpsServer = https.createServer(
      {
        key: privateKey,
        cert: certificate
      },
      app
    );

    // Return HTTPS server instance
    return httpsServer;
  }

  // Return Express server instance
  return app;
};
