/*
 * Copyright (C) 2020 Depwhite Software
 *
 * This file is part of the Depwhite Software project.
 *
 * Depwhite Software project can not be copied and/or distributed without the express
 */
"use strict";

/**
 * Module dependencies.
 */
var _ = require("lodash");
var glob = require("glob");
/**
 * Load app configurations
 */
module.exports = _.extend(
  require("./env/all"),
  require('./env/' + process.env.NODE_ENV) || {}
);

/**
 * Get files by glob patterns
 */
module.exports.getGlobbedFiles = async function (globPatterns, removeRoot) {
  // For context switching
  var _this = this;

  // URL paths regex
  var urlRegex = new RegExp("^(?:[a-z]+:)?//", "i");

  // The output array
  var output = [];
  // If glob pattern is array so we use each pattern in a recursive way, otherwise we use glob
  if (_.isArray(globPatterns)) {
    for (const globPattern of globPatterns) {
      const result = await _this.getGlobbedFiles(globPattern, removeRoot);
      output = _.union(output, result);
    }
    return output;
  } else if (_.isString(globPatterns)) {
    if (urlRegex.test(globPatterns)) {
      output.push(globPatterns);
    } else {
      var files = glob(globPatterns, { sync: true });
      if (removeRoot) {
        files = files.map(function (file) {
          return file.replace(removeRoot, "");
        });
      }
      output = _.union(output, files);
    }
    return output;
  }
};

/**
 * Get the modules JavaScript files
 */
module.exports.getJavaScriptAssets = async function (includeTests) {
  const output = await this.getGlobbedFiles(
    this.assets.lib.js.concat(this.assets.js),
    "public"
  );
  // To include tests
  if (includeTests) {
    output = _.union(output, await this.getGlobbedFiles(this.assets.tests));
  }
  return output;
};

/**
 * Get the modules CSS files
 */
module.exports.getCSSAssets = async function () {
  const output = await this.getGlobbedFiles(
    this.assets.lib.css.concat(this.assets.css),
    "public"
  );
  return output;
};
