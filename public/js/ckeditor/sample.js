﻿/**
 * Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/* exported initSample */

if (CKEDITOR.env.ie && CKEDITOR.env.version < 9)
	CKEDITOR.tools.enableHtml5Elements(document);

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 150;
CKEDITOR.config.width = 'auto';

// TH
var topic_th = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('topic_th');
		// :(((
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('topic_th');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('topic_th');

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};
	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var highlights_th = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('highlights_th');
		// :(((
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('highlights_th');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('highlights_th');

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};
	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();


var health_content_th = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('health_content_th');
		// :(((
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('health_content_th');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('health_content_th');

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};
	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

//EN
var topic_en = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('topic_en');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('topic_en');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('topic_en');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var highlights_en = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('highlights_en');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('highlights_en');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('highlights_en');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();


var health_content_en = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('health_content_en');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('health_content_en');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('health_content_en');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();


//ARA
var topic_ara = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('topic_ara');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('topic_ara');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('topic_ara');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var highlights_ara = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('highlights_ara');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('highlights_ara');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('highlights_ara');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var health_content_ara = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('health_content_ara');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('health_content_ara');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('health_content_ara');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();


//JP
var topic_jp = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');

	return function () {
		var editorElement = CKEDITOR.document.getById('topic_jp');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('topic_jp');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('topic_jp');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var highlights_jp = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('highlights_jp');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('highlights_jp');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('highlights_jp');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();

var health_content_jp = (function () {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get('bbcode');
	return function () {
		var editorElement = CKEDITOR.document.getById('health_content_jp');
		if (isBBCodeBuiltIn) {
			editorElement.setHtml(
				'Hello world!\n\n' +
				'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
			);
		}
		if (wysiwygareaAvailable) {
			CKEDITOR.replace('health_content_jp');
		} else {
			editorElement.setAttribute('contenteditable', 'true');
			CKEDITOR.inline('health_content_jp');
		}
	};
	function isWysiwygareaAvailable() {
		if (CKEDITOR.revision == ('%RE' + 'V%')) {
			return true;
		}
		return !!CKEDITOR.plugins.get('wysiwygarea');
	}
})();





