
/**
 * Services
 */
function numberOnly() {
    key = event.keyCode;
    if ((key != 46) & (key < 48 || key > 57)) event.returnValue = false;
}

function currentURL() {
    return window.location.href;
}

function numberFormat(item) {
    return item.replace(/(\d)(?=(\d{3})+\.\d\d$)/g, "$1,");
}

/**
 * CHANGE ADDRESS
 */
$('select[name="province[]"]').change(function () {
    const provinceID = $(this).val()
    $.get('/provinces/' + provinceID + '/districts', (districts) => {
        console.log(districts)
    })
})

/**
 *  Set Language URL
 */

function setUrl(lang) {
    let url_current = currentURL()
    let url = new URL(url_current);
    let query_string = url.search;
    let search_params = new URLSearchParams(query_string);
    search_params.set('ln', lang);   // new value of "ln" is set to "(gb or th )"
    url.search = search_params.toString();    // change the search property of the main url
    let new_url = url.toString();    // the new url string
    window.location.href = new_url;

}

/**
 * BACK
 */
$('#back').click(function () {
    // url default 
    let url_back = currentURL().substring(0, currentURL().lastIndexOf('/'))
    let new_url = newUrl(url_back)
    window.location = new_url
})

/***
 *  New Url
 */
function newUrl(url_go) {
    // find ln
    const urlParams = new URLSearchParams(window.location.search);
    const myParam = urlParams.get('ln') ? urlParams.get('ln') : 'th';

    let url = new URL(url_go + '?ln=th');
    let query_string = url.search;
    let search_params = new URLSearchParams(query_string);
    search_params.set('ln', myParam);   // new value of "ln" is set to "(gb or th )"
    url.search = search_params.toString();    // change the search property of the main url
    let new_url = url.toString();    // the new url string
    return new_url

}


/**
 * SUBMIT FORM
 */
$('#form-submit').submit(function () {
    CKEDITOR.instances['topic_th'].updateElement();
    CKEDITOR.instances['highlights_th'].updateElement();
    CKEDITOR.instances['health_content_th'].updateElement();

    CKEDITOR.instances['topic_en'].updateElement();
    CKEDITOR.instances['highlights_en'].updateElement();
    CKEDITOR.instances['health_content_en'].updateElement();

    CKEDITOR.instances['topic_ara'].updateElement();
    CKEDITOR.instances['highlights_ara'].updateElement();
    CKEDITOR.instances['health_content_ara'].updateElement();

    CKEDITOR.instances['topic_jp'].updateElement();
    CKEDITOR.instances['highlights_jp'].updateElement();
    CKEDITOR.instances['health_content_jp'].updateElement();

    $('#btn-submit').prop('disabled', true);
    $('#form-submit').ajaxSubmit(function (f) {
        if (f.code == 200) {
            if ($('#form-submit').attr('action').indexOf('_method=PUT') !== -1) {
                let ulr_default = currentURL().substring(0, currentURL().lastIndexOf('/'))
                let url = newUrl(ulr_default)
                app.success('Successfully!', 'Action has been success', 'go', url)

            } else {
                //let ulr_default = currentURL().substring(0, currentURL().lastIndexOf('?'))
                let ulr_default = currentURL().substring(0, currentURL().lastIndexOf('/'))
                let url = newUrl(ulr_default)

                app.success('Successfully!', 'Action has been success', 'go', url)
            }

        } else {
            app.is_invalid(f.data)
            $('#btn-submit').prop('disabled', false);
        }

    });
    return false;
});

/**
 * functional
 */
var input = (title, option, url, data) => {
    app.input(title, option, url, data)
}


/**
 * ALERT
 */
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000
})

var app = {
    is_invalid: function (data) {
        let text = ''
        $('.invalid-feedback').remove()
        $('.form-control').removeClass('is-invalid')
        data.forEach((err) => {
            if (err.param == 'alert') {
                app.error(err.msg)
            } else {
                text = '<div class="invalid-feedback">' + err.msg + '</div>'
                $('[name="' + err.param + '"]').addClass('is-invalid')
                $('[name="' + err.param + '"]').after(text)
                $('[name="' + err.param + '"]').focus()
                if (err.param == 'role_name') {
                    setTimeout(function () {
                        $('html, body').animate({ scrollTop: 0 }, 100);
                    }, 100);//call every 100 miliseconds
                }
            }
        })


    },
    is_valid: function (name) {
        $('[name="' + name + '"]').addClass('invalid')
    },
    /**
     * Sweet Alert
     */
    load: function (url, data) {
        $.post(url, data, function (d) {
            $("#box-modal").html(d);
        });
    },
    error: function (text) {
        swal.fire({
            text: text,
            type: "error",
            buttons: {
                dance: {
                    text: "Close",
                    className: 'btn-smtv'
                }
            }
        });
    },
    success: function (title, text, option, url) {
        Toast.fire({
            title: title,
            text: text,
            type: 'success'
        }).then(() => {
            if (option == "reset") $(url).resetForm();
            else if (option == "reload") window.location.reload();
            else if (option == "go") window.location = url;
        });
    },
    confirm: function (text, option, url, data, callback) {
        Swal.fire({
            title: 'Are you sure?',
            text: text,
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then(confirm => {
            if (confirm.value) {
                if (option == "reload") {
                    window.location.reload();
                } else if (option == "go") {
                    window.location = url;
                } else if (option == "delete") {
                    $.ajax(url, {
                        type: 'DELETE',  // http method
                        data: data,  // data to submit
                        success: function (f, status, xhr) {
                            if (f.code == 200)
                                app.success('Deleted!', 'item has been deleted', 'reload')
                            else
                                Swal.fire({
                                    title: "System is wrong",
                                    text: f.message,
                                    icon: "error"
                                });
                        },
                        error: function (jqXhr, textStatus, errorMessage) {
                            Swal.fire({
                                title: "System is wrong",
                                text: errorMessage,
                                icon: "error"
                            });
                        }
                    });
                } else if (option == "change") {
                    $.ajax({
                        url: url,
                        type: "GET", // DELETE
                        success: function (d) {
                            if (d.code == 200) window.location.reload();
                            else swal(d.message);
                        }
                    });

                } else {
                    callback(true)
                }
            } else {
                callback(false)
            }
        });
    },
    input: function (title, options, url, data) {
        Swal.fire({
            title: title,
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            inputValue: data,
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                if (options == 'update')
                    options = 'PUT'
                else
                    options = 'POST'

                return $.ajax(url, {
                    type: options,  // http method
                    data: { name: text },  // data to submit
                    success: function (f, status, xhr) {
                        console.log(f)
                        if (f.code == 200)
                            return f.code
                        else
                            Swal.showValidationMessage(
                                `${f.message}`
                            )
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                        Swal.showValidationMessage(
                            `Request failed: ${errorMessage}`
                        )
                    }
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                app.success('Updated!', 'item has been Updated', 'reload')
            }
        })
    }
}

